class Figure {
    private String mColor;

    public Figure(String color) {
        mColor = color;
    }

    public String getColor() {
        return mColor;
    }
}
class Rectangle extends Figure{
    private int mStorona1;
    private int mStorona2;
    public Rectangle(String color,int storona1,int storona2) {
        super(color);
        mStorona1 = storona1;
        mStorona2 = storona2;
    }
    public void getSquare() {
        System.out.println("Square:"+mStorona1*mStorona2);
    }
}

class Parallelogram extends Figure{
    private int mStorona;
    private int mVisota;
    public Parallelogram(String color,int storona,int visota) {
        super(color);
        mStorona = storona;
        mVisota = visota;
    }
    public void getSquare() {
        System.out.println("Square:"+mStorona*mVisota);
    }
}

class Circle extends Figure{
    public final double pi = 3.14159265359;
    private int mRadius;
    public Circle(String color,int radius) {
        super(color);
        mRadius = radius;
    }
    public void getSquare() {
        System.out.println("Square:"+mRadius*mRadius*pi);
    }
}


public class Main {

    public static void main(String[] args) {
        Rectangle re1 = new Rectangle("Red",2,3);
        System.out.println(re1.getClass());
        System.out.println("Color:"+re1.getColor());
        re1.getSquare();

        System.out.println();

        Circle ci1 = new Circle("Black",1);
        System.out.println(ci1.getClass());
        System.out.println("Color:"+ci1.getColor());
        ci1.getSquare();

        System.out.println();

        Parallelogram pa1 = new Parallelogram("Grey",4,2);
        System.out.println(pa1.getClass());
        System.out.println("Color:"+pa1.getColor());
        pa1.getSquare();
    }
}



